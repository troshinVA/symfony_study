<?php


namespace App\Service;


use Demontpx\ParsedownBundle\Parsedown;
use Psr\Log\LoggerInterface;
use Symfony\Component\Cache\Adapter\AdapterInterface;
use Symfony\Component\Security\Core\Security;

class MarkDownParser
{
    /**
     * @var Parsedown
     */
    private $parsedown;
    /**
     * @var AdapterInterface
     */
    private $cache;
    /**
     * @var LoggerInterface
     */
    private $logger;
    /**
     * @var Security
     */
    private $security;

    /**
     * MarkDownParser constructor.
     * @param Parsedown $parsedown
     * @param AdapterInterface $cache
     * @param LoggerInterface $markdownLogger
     * @param Security $security
     */
    public function __construct(
        Parsedown $parsedown,
        AdapterInterface $cache,
        LoggerInterface $markdownLogger,
        Security $security
    ) {

        $this->parsedown = $parsedown;
        $this->cache = $cache;
        $this->logger = $markdownLogger;
        $this->security = $security;
    }

    public function parse(string $source): string
    {
        if (stripos($source, 'Товарищи!') !== false) {
            $this->logger->info('Внимание!', [
                'user' => $this->security->getUser(),
            ]);
        }

        return $this->cache->get(
            'markdown ' . md5($source),
            function () use ($source) {
                return $this->parsedown->text($source);
            }
        );
    }
}