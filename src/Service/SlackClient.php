<?php


namespace App\Service;


use Http\Client\Exception;
use Nexy\Slack\Client;
use Nexy\Slack\Exception\SlackApiException;

class SlackClient
{
    /**
     * @var Client
     */
    private $slack;

    /**
     * SlackClient constructor.
     * @param Client $slack
     */
    public function __construct(Client $slack)
    {
        $this->slack = $slack;
    }

    /**
     * @param string $message
     * @param string $icon
     * @param string $from
     * @return Client
     * @throws Exception
     * @throws SlackApiException
     */
    public function send(string $message, string $icon = ':ghost:', string $from = 'Ghost')
    {
        $slackMessage = $this->slack->createMessage();

        $slackMessage
            ->from($from)
            ->withIcon($icon)
            ->setText($message);

        $this->slack->sendMessage($slackMessage);
    }
}