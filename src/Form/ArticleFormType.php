<?php

namespace App\Form;

use App\Entity\Article;

use App\Entity\User;
use App\Repository\UserRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ArticleFormType extends AbstractType
{
    /**
     * @var UserRepository
     */
    private $userRepository;


    /**
     * ArticleFormType constructor.
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var Article|null $article */
        $article = $options['data'] ?? null;

        $cannotEditAuthor = $article && $article->getId() && $article->isPublished();

        $builder
            ->add('title', TextType::class, [
                'label' => 'Укажите название статьи',
                'help' => 'Не используйте цифры в названии',
                'required' => false
            ])
            ->add('body', null, [
                'rows' => 15,
            ])
            ->add('author', EntityType::class, [
                'class' => User::class,
                'choice_label' => function (User $user) {
                    return sprintf('%s (id: %d)', $user->getFirstName(), $user->getId());
                },
                'placeholder' => 'Выберите автора статьи',
                'choices' => $this->userRepository->findAllSortedByName(),
                'disabled' => $cannotEditAuthor,
            ]);

        if ($options['enable_published_at']) {
            $builder
                ->add('publishedAt', null, [
                    'widget' => 'single_text'
                ]);
        }

        // пример трансформации данных при отправке формы
        /*        $builder->get('body')
                    ->addModelTransformer(new CallbackTransformer(
                        function ($bodyFromDataBase) {
                            return str_replace('**собака**', 'собака', $bodyFromDataBase);
                        },  // функция преобразует данные из базы для отправки в форму
                        function ($bodyFromInput) {
                            return str_replace('собака', '**собака**', $bodyFromInput);
                        } // преобразовывает данные из формы для отправки в базу
                    ));*/
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Article::class,
            'enable_published_at' => false
        ]);
    }
}
