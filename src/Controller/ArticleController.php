<?php

namespace App\Controller;

use App\Entity\Article;
use App\Repository\ArticleRepository;
use App\Service\SlackClient;
use Http\Client\Exception;
use Nexy\Slack\Exception\SlackApiException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ArticleController extends AbstractController
{
    /**
     * @Route("/", name="app_homepage")
     * @param ArticleRepository $repository
     * @return Response
     */
    public function homepage(ArticleRepository $repository): Response
    {
        $articles = $repository->findLatestPublished();
        return $this->render('articles/homepage.html.twig', [
            'articles' => $articles
        ]);
    }

    /**
     * @Route("/articles/{slug}", name="app_article")
     * @param Article $article
     * @param SlackClient $slackClient
     * @return Response
     * @throws Exception
     * @throws SlackApiException
     */
    public function show(Article $article, SlackClient $slackClient)
    {
        // отправка сообщения в slack ------------
        if ($article->getSlug() == 'slack') {
            $slackClient->send('This is an amazing message!');
        }
        //------------------

        // Первый способ работы с кэшем
        /*        $item = $cache->getItem('markdown ' . md5($articleContent));

                if (!$item->isHit()) {
                    $item->set($parsedown->text($articleContent));
                    $cache->save($item);
                }
                $articleContent = $item->get();*/

        // Второй способ работы с кэшем
        /*        $articleContent = $cache->get(
                    'markdown ' . md5($articleContent),
                    function () use ($parsedown, $articleContent) {
                        return $parsedown->text($articleContent);
                    }
                );*/
        return $this->render('articles/show.html.twig', [
            'article' => $article
        ]);
    }
}