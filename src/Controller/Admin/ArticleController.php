<?php

namespace App\Controller\Admin;

use App\Entity\Article;
use App\Entity\User;
use App\Form\ArticleFormType;
use App\Repository\ArticleRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ArticleController
 * @package App\Controller\Admin
 * @method User|null getUser()
 */
class ArticleController extends AbstractController
{
    /**
     * @IsGranted ("ROLE_ADMIN_ARTICLE")
     * @Route ("/admin/articles", name="admin_articles")
     * @param ArticleRepository $articleRepository
     * @param Request $request
     * @param PaginatorInterface $paginator
     * @return Response
     */
    public function index(ArticleRepository $articleRepository, Request $request, PaginatorInterface $paginator): Response
    {
        $pagination = $paginator->paginate(
            $articleRepository->latest(),
            $request->query->getInt('page', 1),
            10
        );

        return $this->render('admin/articles/index.html.twig', [
            'pagination' => $pagination
        ]);
    }

    /**
     * @Route("/admin/articles/create", name="admin_articles_create")
     * @param EntityManagerInterface $em
     * @return Response
     * @throws \Exception
     * @IsGranted("ROLE_ADMIN_ARTICLE")
     */
    public function create(EntityManagerInterface $em, Request $request): Response
    {
        $form = $this->createForm(ArticleFormType::class);

        if ($article = $this->handleFormRequest($form, $em, $request)) {

            $this->addFlash('flash_message', 'Статья успешно создана');

            return $this->redirectToRoute('admin_articles');
        }

        return $this->render('admin/articles/create.html.twig', [
            'articleForm' => $form->createView(),
            'showError' => $form->isSubmitted()
        ]);
    }

    /**
     * @param Article $article
     * @param EntityManagerInterface $em
     * @param Request $request
     * @return Response
     * @Route("/admin/articles/{id}/edit", name="admin_articles_edit")
     * @IsGranted("MANAGE", subject="article")
     */
    public function edit(Article $article, EntityManagerInterface $em, Request $request): Response
    {
        $form = $this->createForm(ArticleFormType::class, $article, [
            'enable_published_at' => true,
        ]);

        if ($article = $this->handleFormRequest($form, $em, $request)) {

            $this->addFlash('flash_message', 'Статья успешно изменена');

            return $this->redirectToRoute('admin_articles_edit', ['id' => $article->getId()]);
        }

        return $this->render('admin/articles/edit.html.twig', [
            'articleForm' => $form->createView(),
            'showError' => $form->isSubmitted()
        ]);
    }

    private function handleFormRequest(FormInterface $form, EntityManagerInterface $em, Request $request)
    {
        $form->handleRequest($request); // проверяет происходила ли отправка формы, хэндлер знает каким методом форма была отправлена и какие данные должны прийти

        if ($form->isSubmitted() && $form->isValid()) {  // isSubmitted возвращает информацию была ли отправлена форма, а isValid прошла ли форма валидацию
            /** @var Article $article */
            $article = $form->getData(); // getData возвращает объект который привязан к форме, если объект не привязан возвращается ассоциативный массив

            $em->persist($article);
            $em->flush();

            return $article;
        }

        return null;
    }
}
