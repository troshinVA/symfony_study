<?php


namespace App\Twig;


use App\Service\MarkDownParser;
use Twig\Extension\RuntimeExtensionInterface;

class AppRuntime implements RuntimeExtensionInterface
{
    /**
     * @var MarkDownParser
     */
    private $markDownParser;

    /**
     * AppExtension constructor.
     * @param MarkDownParser $markDownParser
     */
    public function __construct(MarkDownParser $markDownParser)
    {
        $this->markDownParser = $markDownParser;
    }

    public function parseMarkdown($content): string
    {
        return $this->markDownParser->parse($content);
    }
}