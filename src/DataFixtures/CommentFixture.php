<?php

namespace App\DataFixtures;

use App\Entity\Article;
use App\Entity\Comment;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class CommentFixture extends BaseFixtures implements DependentFixtureInterface
{
    public function loadData(ObjectManager $manager)
    {
        $this->createMany(Comment::class, 100, function (Comment $comment) {
            $comment
                ->setAuthorName($this->faker->realText(15))
                ->setContent($this->faker->paragraph)
                ->setArticle($this->getRandomReference(Article::class))
                ->setCreatedAt($this->faker->dateTimeBetween('-100 days', '-1 days'));

            if ($this->faker->boolean(50)) {
                $comment->setDeletedAt($this->faker->dateTimeThisMonth);
            }
        });

        $manager->flush();
    }

    public function getDependencies(): array
    {
        return [
            ArticleFixtures::class
        ];
    }
}
